'use strict';

let users = {};
let createButton = document.querySelector('#createButton');
let renameButton = document.querySelector('#renameButton');
let removeButton = document.querySelector('#removeButton');
let nameField = document.querySelector('#nameField');
let usersBlock = document.querySelector('#usersBlock');

function init () {
    users[1] = new User(1); // Создаем объект, вызываем функцию конструктор, объект записываем в ключ 1 объекта users
    users[1].setUserName('Bob'); // с помощью публичного метода записываем имя в ключ 1 объекта users
    users[2] = new User(2);
    users[2].setUserName('John');
    users[3] = new User(3);
    users[3].setUserName('Fred');
}

function User (id) {
    let userName = '';
    let userId = id;
    let card = document.createElement('div');
    let p = document.createElement('p');

    this.setUserName = function (name) {
        userName = name;
        p.textContent = userName;
    };

    card.classList.add('card'); // добавляем элементу card класс card
    userName = nameField.value; // записываем значение инпута в юзернейм
    p.textContent = userName; // записываем юзернейм в текстовое содержимое элемента p
    card.appendChild(p); // добавляем элемент р в конец списка дочерних элементов card
    usersBlock.appendChild(card); // добавляем элемент card в конец списка дочерних элементов usersBlock


    card.addEventListener('click', function () { // при клике на card..
        removeButton = removeAllListeners(removeButton); // удаляем все Listeners с кнопки removeButton
        renameButton = removeAllListeners(renameButton); // удаляем все Listeners с кнопки renameButton
        renameButton.innerText = 'RENAME'; // задаем текстовое значение "RENAME" элементу (кнопке) renameButton
        removeButton.innerText = 'REMOVE'; // задаем текстовое значение "REMOVE" элементу (кнопке) removeButton
        nameField.value = userName; // записываем значение юзернейма в значение инпута
        createButton.style.display = 'none';
        renameButton.style.display = 'inline-block';
        removeButton.style.display = 'inline-block';

        renameButton.addEventListener('click', function (event) { // при клике на renameButton..
            event.preventDefault(); // предотвращаем перезагрузку страницы
            userName = nameField.value; // записываем значение инпута в юзернейм
            p.textContent = userName; // записываем юзернейм в текстовое значение элемента p
            nameField.value = ''; // очищаем инпут
            createButton.style.display = 'inline-block';
            renameButton.style.display = 'none';
            removeButton.style.display = 'none';
        });

        removeButton.addEventListener('click', function (event) { // при клике на removeButton..
            removeButton = removeAllListeners(removeButton); // удаляем все Listeners с кнопки removeButton
            event.preventDefault(); // предотвращаем перезагрузку страницы
            nameField.value = ''; // очищаем инпут
            usersBlock.removeChild(card); // удаляем дочерний элемент card родителя usersBlock
            // this.parentNode.remove();
            delete users[userId]; // с помощью оператора delete ключ userId из объекта users
            removeButton.style.display = 'none';
            renameButton.style.display = 'none';
            createButton.style.display = 'inline-block';
        })
    });

    function removeAllListeners(element) { // функция удаляет все Listeners
        let newElement = element.cloneNode(); // клонируем element в newElement
        element.parentNode.replaceChild(newElement, element); // заменяем  элемент element элементом newElement и возвращаем newElement
        return newElement
    }
}

createButton.addEventListener('click', function (event) {
    if (nameField.value !== '') { // проверка на пустое поле, ввел ли пользователь что-то
        event.preventDefault();
        let date = new Date; // Создаем объект и вызываем функцию конструктор, объект записываем в дату
        let hash = date.getTime(); // Вызываем публичный метод getTime который возвращает нам дату в формате количества миллисекунд с 1970
        let resultName = 'users' + hash; //Создаем ключ, в который записываем значение которое нам вернул getTime и строку 'users' чтобы получить ключ вида "user121232345423242134"
        users[resultName] = new User(resultName); //Создаем новый объет, выполняем в нем функцию конструктор User и записываем объект с ключом resultName
        nameField.value = ''; // очищаем поле инпута для следующего ввода
    } else {
        return
    }
});

init();

